from matrix_client.client import MatrixClient
from matrix_client.room import Room
from threading import Thread
import time
import re
import os
import json
from config import *
from zerotalk import createWebsocket, postZeroTalk, upvoteZeroTalk, getZeroTalkUserData
import markdown
from markdown.extensions.fenced_code import FencedBlockPreprocessor

AVATAR = "mxc://matrix.org/GyMunQcXmjyygFXxacXkOofw"

# Make python-markdown consistent with CommonMark
FencedBlockPreprocessor.LANG_TAG = " class=\"language-%s\""

client = MatrixClient("https://matrix.org")

print("Logging in...", flush=True)
client.login("zerotalk_bot", "K75HfWsArP01T3jotwR5", sync=True)
print("Logged in", flush=True)

def onInvite(room_id, state):
    time.sleep(5)  # no idea why, but this is required
    client.join_room(room_id)
    time.sleep(1)  # no idea why, but this is required
    room = Room(client, room_id)
    # Greeting
    room.send_html(
        "Hello, I'm a ZeroTalk bridge bot. Use <code>!zerotalk help</code> " +
        "for an overfiew or <code>!zerotalk bridge zerotalk_site_address " +
        "topic_uri</code> to bridge a ZeroTalk topic.",
        msgtype="m.notice"
    )
    room.add_listener(onEvent)


zerotalk_check_threads = {}

def ensureThread(room):
    unensureThread(room)

    tags = room.get_tags()["tags"]
    if "zerotalk_sync" in tags:
        site_address = tags["zerotalk_sync"]["site_address"]
        topic_uri = tags["zerotalk_sync"]["topic_uri"]
        names_enabled = tags["zerotalk_sync"].get("names_enabled", True)
    else:
        # We should't get here actually
        return

    try:
        ws = createWebsocket(site_address)
    except Exception as e:
        print("Failed to create websocket", e, flush=False)
        return

    def watch():
        time.sleep(5)

        def updateComments():
            # Load current room state
            room_messages = [e for e in room.get_events() if e["type"] == "m.room.message"]
            tss = [m["origin_server_ts"] for m in room.get_events()]
            first_ts = 0 if tss == [] else min(tss) / 1000
            now_ts = int(time.time())
            # We can treat all old enough messages (i.e. before first_ts) as
            # already synced (and in case they aren't, too much time has passed
            # so syncing them will most likely make little sense)

            ws.send(json.dumps({
                "cmd": "dbQuery",
                "params": [
                    """
                        SELECT * FROM comment
                        LEFT JOIN json AS data_json ON (
                            data_json.json_id = comment.json_id
                        )
                        LEFT JOIN json AS content_json ON (
                            content_json.directory = data_json.directory AND
                            content_json.file_name = "content.json"
                        )
                        LEFT JOIN keyvalue ON (
                            keyvalue.json_id = content_json.json_id AND
                            keyvalue.key = "cert_user_id"
                        )
                        WHERE (
                            topic_uri = :topic_uri AND
                            added >= :first_ts AND added <= :now_ts AND
                            (
                                -- A silly attempt to prevent infinite
                                -- bidirectional syncing
                                value LIKE "%.bit" OR
                                value LIKE "%.yo"
                            ) AND
                            value != "matrixbridge@zeroid.bit"
                        )
                    """,
                    {
                        "topic_uri": topic_uri,
                        "first_ts": first_ts,
                        "now_ts": now_ts
                    }
                ],
                "id": 1
            }))
            res = json.loads(ws.recv())
            while res["cmd"] != "response":
                res = json.loads(ws.recv())
            for comment in res["result"]:
                # Check whether this message was mirrored already (if it's
                # rather new, it's probably in locally stored event list)
                if not any(
                    m["sender"] == "@zerotalk_bot:matrix.org" and
                    m["content"].get("zerotalk_mapping", {}).get("address") == comment["directory"] and
                    m["content"].get("zerotalk_mapping", {}).get("comment_id") == comment["comment_id"]
                    for m in room_messages
                ):
                    try:
                        # Prepare comment data
                        author = comment["value"]
                        body = comment["body"]
                        if names_enabled:
                            client.api.send_state_event(
                                room.room_id,
                                "m.room.member",
                                {
                                    "membership": "join",
                                    "avatar_url": AVATAR,
                                    "displayname": author
                                },
                                "@zerotalk_bot:matrix.org"
                            )
                            comment_body = body
                        else:
                            client.api.send_state_event(
                                room.room_id,
                                "m.room.member",
                                {
                                    "membership": "join",
                                    "avatar_url": AVATAR,
                                    "displayname": "ZeroTalk Bot"
                                },
                                "@zerotalk_bot:matrix.org"
                            )
                            sep = "\n\n" if body.strip().startswith(">") or body.strip().startswith("```") else " "
                            comment_body = f"**{author}**:{sep}{body}"
                        client.api.send_message_event(
                            room.room_id,
                            "m.room.message",
                            {
                                "msgtype": "m.text",
                                "body": comment_body,
                                "zerotalk_mapping": {
                                    "address": comment["directory"],
                                    "comment_id": comment["comment_id"]
                                },
                                "format": "org.matrix.custom.html",
                                "formatted_body": markdown.markdown(comment_body, extensions=["fenced_code"])
                            }
                        )
                    except Exception as e:
                        print(e, flush=True)

        def updateUpvotes():
            # Load current room state
            room_messages = [e for e in room.get_events() if e["type"] == "m.room.message" and "zerotalk_mapping" in e["content"]]
            room_upvotes = [e for e in room.get_events() if e["type"] == "m.reaction" and "zerotalk_mapping" in e["content"]]
            tss = [m["origin_server_ts"] for m in room.get_events()]
            first_ts = 0 if tss == [] else min(tss) / 1000
            now_ts = int(time.time())
            # We can treat all old enough messages (i.e. before first_ts) as
            # already synced (and in case they aren't, too much time has passed
            # so syncing them will most likely make little sense)

            ws.send(json.dumps({
                "cmd": "dbQuery",
                "params": [
                    """
                        SELECT
                            comment_vote.*,
                            data_json.directory,
                            keyvalue.value
                        FROM comment_vote
                        LEFT JOIN json AS data_json ON (
                            data_json.json_id = comment_vote.json_id
                        )
                        LEFT JOIN json AS content_json ON (
                            content_json.directory = data_json.directory AND
                            content_json.file_name = "content.json"
                        )
                        LEFT JOIN keyvalue ON (
                            keyvalue.json_id = content_json.json_id AND
                            keyvalue.key = "cert_user_id"
                        )
                        LEFT JOIN (
                            SELECT * FROM comment
                            LEFT JOIN json ON (json.json_id = comment.json_id)
                        ) AS comment ON (
                            comment.comment_id || "_" || comment.directory = comment_vote.comment_uri
                        )
                        LEFT JOIN (
                            SELECT * FROM topic
                            LEFT JOIN json ON (json.json_id = topic.json_id)
                        ) AS topic ON (
                            topic.topic_id || "_" || topic.directory = comment.topic_uri
                        )
                        WHERE (
                            topic_uri = :topic_uri AND
                            comment_vote.added >= :first_ts AND
                            comment_vote.added <= :now_ts AND
                            (
                                -- A silly attempt to prevent infinite
                                -- bidirectional syncing
                                value LIKE "%.bit" OR
                                value LIKE "%.yo"
                            ) AND
                            value != "matrixbridge@zeroid.bit"
                        )
                    """,
                    {
                        "topic_uri": topic_uri,
                        "first_ts": first_ts,
                        "now_ts": now_ts
                    }
                ],
                "id": 1
            }))
            res = json.loads(ws.recv())
            while res["cmd"] != "response":
                res = json.loads(ws.recv())
            for upvote in res["result"]:
                # Check whether this upvote was mirrored already (if it's rather
                # new, it's probably in locally stored event list)
                if not any(
                    m["sender"] == "@zerotalk_bot:matrix.org" and
                    m["type"] == "m.reaction" and
                    m["content"].get("zerotalk_mapping", {}).get("address") == upvote["directory"] and
                    m["content"].get("zerotalk_mapping", {}).get("comment_uri") == upvote["comment_uri"]
                    for m in room_upvotes
                ):
                    try:
                        author = upvote["value"]
                        # Check whether the upvote is native (i.e. a ZeroTalk user
                        # upvotes a ZeroTalk message)
                        for message in room_messages:
                            address = message["content"].get("zerotalk_mapping", {}).get("address", "")
                            comment_id = message["content"].get("zerotalk_mapping", {}).get("comment_id", "")
                            if f"{comment_id}_{address}" == upvote["comment_uri"]:
                                # Prepare upvote data
                                client.api.send_message_event(
                                    room.room_id,
                                    "m.reaction",
                                    {
                                        "zerotalk_mapping": {
                                            "address": upvote["directory"],
                                            "comment_uri": upvote["comment_uri"]
                                        },
                                        "m.relates_to": {
                                            "event_id": message["event_id"],
                                            "key": f"🔺 {author}",
                                            "rel_type": "m.annotation"
                                        }
                                    }
                                )
                                break
                        else:
                            # A ZeroTalk user upvotes a Matrix message. That's going
                            # to be a bit more difficult
                            comment_id, address = upvote["comment_uri"].split("_")
                            with open(f"ZeroNet/data/{site_address}/data/users/{address}/data.json") as f:
                                data = json.loads(f.read())
                            event_id = data["comment_metadata"][comment_id]["event_id"]
                            client.api.send_message_event(
                                room.room_id,
                                "m.reaction",
                                {
                                    "zerotalk_mapping": {
                                        "address": upvote["directory"],
                                        "comment_uri": upvote["comment_uri"]
                                    },
                                    "m.relates_to": {
                                        "event_id": event_id,
                                        "key": f"🔺 {author}",
                                        "rel_type": "m.annotation"
                                    }
                                }
                            )
                    except Exception as e:
                        print(e, flush=True)

        def update():
            try:
                updateComments()
            except Exception as e:
                print(e, flush=True)
            try:
                updateUpvotes()
            except Exception as e:
                print(e, flush=True)

        ws.send(json.dumps({
            "cmd": "channelJoin",
            "params": ["siteChanged"],
            "id": -1
        }))
        ws.recv()

        update()
        while True:
            res = json.loads(ws.recv())
            if res["cmd"] == "setSiteInfo" and "event" in res["params"]:
                event = res["params"]["event"]
                if event[0] == "file_done":
                    update()

    t = Thread(target=watch)
    t.daemon = True
    t.start()
    zerotalk_check_threads[room.room_id] = ws

def unensureThread(room):
    if room.room_id in zerotalk_check_threads:
        zerotalk_check_threads[room.room_id].close()
        del zerotalk_check_threads[room.room_id]

def onEvent(room, event):
    try:
        if event["type"] == "m.room.message" and event["content"]["msgtype"] == "m.text":
            power_levels = client.api.get_power_levels(room.room_id)
            power_level = power_levels.get("users", {}).get(event["sender"]) or power_levels.get("users_default") or 0
            if event["content"]["body"] == "!zerotalk help":
                room.send_html(
                    "<ul>\n" +
                    "<li><code>!zerotalk bridge zerotalk_site_address " +
                    "topic_uri</code> to bridge a topic</li>\n" +
                    "<li><code>!zerotalk unbridge</code> to unbridge</li>\n" +
                    "<li><code>!zerotalk status</code> to get room " +
                    "status</li>\n" +
                    "<li><code>!zerotalk nice</code> to embed " +
                    "usernames</li>\n" +
                    "<li><code>!zerotalk unnice</code> to use static " +
                    "username</li>",
                    msgtype="m.notice"
                )
            elif event["content"]["body"].startswith("!zerotalk bridge "):
                if power_level < 50:
                    room.send_html(
                        "You must be at least a moderator to bridge.",
                        msgtype="m.notice"
                    )
                    return
                args = event["content"]["body"].split()
                if len(args) != 4:
                    room.send_html(
                        "Use <code>!zerotalk bridge zerotalk_site_address " +
                        "topic_uri</code> to bridge a topic.",
                        msgtype="m.notice"
                    )
                    return
                site_address = args[2]
                topic_uri = args[3]
                if not re.match("^[A-Za-z0-9]{26,35}$", site_address):
                    room.send_html(
                        "The site address is invalid. Please make sure you're " +
                        "using an address, not a domain.",
                        msgtype="m.notice"
                    )
                    return
                if not re.match("^\d+_[A-Za-z0-9]{26,35}$", topic_uri):
                    room.send_html(
                        "The topic URI is invalid. Correct example: " +
                        "1567166183_1Cy3ntkN2GN9MH6EaW6eHpi4YoRS2nK5Di.",
                        msgtype="m.notice"
                    )
                    return

                # Add new one
                room.add_tag("zerotalk_sync", content={
                    "site_address": site_address,
                    "topic_uri": topic_uri,
                    "names_enabled": True
                })

                room.send_html(
                    "The bridge was set successfully! If you are the owner of " +
                    "the forum, please consider adding " +
                    "1M81LbVpscGocBAHbeRTu9Hm9cx6zCoNTu as a signer of " +
                    "data/users/content.json, like this (see root content.json):" +
                    "<pre>" +
                    "\"includes\": {\n" +
                    "    \"data/users/content.json\": {\n" +
                    "        \"signers\": [\"1M81LbVpscGocBAHbeRTu9Hm9cx6zCoNTu\"],\n" +
                    "        \"signers_required\": 1\n" +
                    "    }\n" +
                    "},\n" +
                    "</pre>" +
                    "This will ensure that homeservers are directly mapped to " +
                    "ZeroNet accounts which are generated on the fly instead of " +
                    "using a single matrixzerotalkbot@zeroid.bit account for " +
                    "everyone.\n\n" +
                    "NB: The bridge is currently configured in 'nice' mode, " +
                    "which works well with Matrix clients but might work worse " +
                    "with other bridges. You can disable 'nice' mode with " +
                    "<code>!zerotalk unnice</code>.\n\n"+
                    "Use <code>!zerotalk unbridge</code> if you want to stop " +
                    "syncing.",
                    msgtype="m.notice"
                )

                ensureThread(room)
            elif event["content"]["body"] == "!zerotalk unbridge":
                if power_level < 50:
                    room.send_html(
                        "You must be at least a moderator to unbridge.",
                        msgtype="m.notice"
                    )
                    return
                room.remove_tag("zerotalk_sync")
                room.send_html("Unbridged.", msgtype="m.notice")
                unensureThread(room)
            elif event["content"]["body"] == "!zerotalk unnice":
                if power_level < 50:
                    room.send_html(
                        "You must be at least a moderator to change settings.",
                        msgtype="m.notice"
                    )
                    return

                tag = room.get_tags()["tags"].get("zerotalk_sync")
                if not tag:
                    room.send_html("Not bridged.", msgtype="m.notice")
                    return

                room.add_tag("zerotalk_sync", content={
                    "site_address": tag["site_address"],
                    "topic_uri": tag["topic_uri"],
                    "names_enabled": False
                })
                ensureThread(room)
                room.send_html(
                    "Changed nice mode. You can use <code>!zerotalk nice</code> " +
                    "to cancel this.",
                    msgtype="m.notice"
                )
            elif event["content"]["body"] == "!zerotalk nice":
                if power_level < 50:
                    room.send_html(
                        "You must be at least a moderator to change settings.",
                        msgtype="m.notice"
                    )
                    return

                tag = room.get_tags()["tags"].get("zerotalk_sync")
                if not tag:
                    room.send_html("Not bridged.", msgtype="m.notice")
                    return

                room.add_tag("zerotalk_sync", content={
                    "site_address": tag["site_address"],
                    "topic_uri": tag["topic_uri"],
                    "names_enabled": True
                })
                ensureThread(room)
                room.send_html(
                    "Changed nice mode. You can use <code>!zerotalk " +
                    "unnice</code> to cancel this.",
                    msgtype="m.notice"
                )
            elif event["content"]["body"] == "!zerotalk status":
                tags = room.get_tags()["tags"]
                if "zerotalk_sync" in tags:
                    site_address = tags["zerotalk_sync"]["site_address"]
                    topic_uri = tags["zerotalk_sync"]["topic_uri"]
                    room.send_html(f"This room is bridged to <a href='http://127.0.0.1:43110/{site_address}/?Topic:{topic_uri}'>{site_address}</a>", msgtype="m.notice")
                else:
                    room.send_html("This room is not bridged.", msgtype="m.notice")
            else:
                if event["sender"] != "@zerotalk_bot:matrix.org":
                    body = event["content"]["body"]
                    event_id = event["event_id"]
                    # Check whether it's an edit
                    if "m.relates_to" in event["content"] and event["content"]["m.relates_to"].get("rel_type") == "m.replace" and event["content"]["m.new_content"]["msgtype"] == "m.text":
                        body = event["content"]["m.new_content"]["body"]
                        event_id = event["content"]["m.relates_to"]["event_id"]

                    tags = room.get_tags()["tags"]
                    if "zerotalk_sync" in tags:
                        site_address = tags["zerotalk_sync"]["site_address"]
                        topic_uri = tags["zerotalk_sync"]["topic_uri"]
                        # Setup a separate thread
                        def publish():
                            postZeroTalk(site_address, topic_uri, event["sender"], body, event_id)
                        t = Thread(target=publish)
                        t.daemon = True
                        t.start()
        elif event["type"] == "m.room.redaction":
            if event["sender"] != "@zerotalk_bot:matrix.org":
                event_id = event["redacts"]

                tags = room.get_tags()["tags"]
                if "zerotalk_sync" in tags:
                    site_address = tags["zerotalk_sync"]["site_address"]
                    topic_uri = tags["zerotalk_sync"]["topic_uri"]
                    # Setup a separate thread
                    def publish():
                        postZeroTalk(site_address, topic_uri, event["sender"], None, event_id)
                    t = Thread(target=publish)
                    t.daemon = True
                    t.start()
        elif event["type"] == "m.reaction":
            if event["sender"] != "@zerotalk_bot:matrix.org":
                if event["content"]["m.relates_to"]["key"] == "👍":
                    event_id = event["content"]["m.relates_to"]["event_id"]

                    tags = room.get_tags()["tags"]
                    if "zerotalk_sync" in tags:
                        site_address = tags["zerotalk_sync"]["site_address"]
                        topic_uri = tags["zerotalk_sync"]["topic_uri"]
                        # Setup a separate thread
                        def publish():
                            # Find the event we're looking for
                            for upvoted_event in room.get_events():
                                if upvoted_event["event_id"] == event_id:
                                    if "zerotalk_mapping" in upvoted_event["content"]:
                                        # A Matrix user upvotes a ZeroTalk comment
                                        address = upvoted_event["content"]["zerotalk_mapping"].get("address", "")
                                        comment_id = upvoted_event["content"]["zerotalk_mapping"].get("comment_id", "")
                                        comment_uri = f"{comment_id}_{address}"
                                        upvoteZeroTalk(site_address, event["sender"], comment_uri)
                                    else:
                                        # A Matrix user upvotes a Matrix comment
                                        data = getZeroTalkUserData(site_address, upvoted_event["sender"])
                                        address = data["_addr"]
                                        for comment_id in data.get("comment_metadata", {}):
                                            if data["comment_metadata"][comment_id]["event_id"] == event_id:
                                                comment_uri = f"{comment_id}_{address}"
                                                upvoteZeroTalk(site_address, event["sender"], comment_uri)
                                    break


                        t = Thread(target=publish)
                        t.daemon = True
                        t.start()
    except Exception as e:
        print(e, flush=True)

client.add_invite_listener(onInvite)
for room in client.get_rooms().values():
    print("Adding room", room, flush=True)
    room.add_listener(onEvent)
    ensureThread(room)

client.start_listener_thread()

print("Listening", flush=True)
time.sleep(30 * 60)

print("Done", flush=True)
raise SystemExit(0)